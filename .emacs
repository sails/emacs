;;**********************	basic setting	*********************
 
;;Load_path
(add-to-list 'load-path' "~/.emacs.d/plugins")

(load-file "~/.emacs.d/plugins/common.el")
(load-file "~/.emacs.d/plugins/common-programming.el") 
(load-file "~/.emacs.d/plugins/set-key.el")
