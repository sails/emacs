(add-to-list 'load-path "~/.emacs.d/plugins/program")

;;代码折叠i
(load-library "hideshow")
(add-hook 'java-mode-hook 'hs-minor-mode)
(add-hook 'perl-mode-hook 'hs-minor-mode)
(add-hook 'php-mode-hook 'hs-minor-mode)
(add-hook 'emacs-lisp-mode-hook 'hs-minor-mode)
(global-set-key (kbd "C-=") 'hs-toggle-hiding)


(setq default-tab-width 4)

;;======================                常用编程插件                    =====================

;;**********************                        template                        *********************
(require 'template)
(template-initialize)
(setq template-default-directories (cons "~/.emacs.d/templates/" template-default-directories))

;;**********************                        popup                        *********************
(add-to-list 'load-path "~/.emacs.d/plugins/popup-el")
;;**********************                        popup                        *********************
;;======================                        Load cedet                      =====================
(semantic-mode 1)
(global-ede-mode t)
;;----------------------                        END cedet                           ---------------------

;;======================                        Load ecb                                =====================
(add-to-list 'load-path "~/.emacs.d/plugins/ecb")

(require 'ecb)
(custom-set-variables '(ecb-options-version "2.40"))
(setq ecb-tip-of-the-day nil)
;;设置可用鼠标点击
(custom-set-variables
 '(ecb-primary-secondary-mouse-buttons (quote mouse-1--C-mouse-1)))
(custom-set-faces
 )


;;打开emacs，然后M-x ecb-activate/M-x ecb-deactivate打开和关闭,M-left,M-right,M-up,M-down可以在各个窗口之间进行切换。
(global-set-key [M-left] 'windmove-left)
(global-set-key [M-right] 'windmove-right)
(global-set-key [M-up] 'windmove-up)
(global-set-key [M-down] 'windmove-down)
;;----------------------                        END     ecb                             ---------------------


;;======================                        Load gdb-many-window                   =====================
;;gdb-many-window
;;这个功能可以使emacs的调试界面像VC一样，有watch, stacktrace等窗口，真正实现图形化gdb.
;;在emacs中编译好程序，然后M-x gdb，多窗口gdb就出来了
(setq gdb-many-windows t)
;;----------------------                        END     gdb-many-window                 ---------------------


;;======================                       quickrun                            =====================
(add-to-list 'load-path "~/.emacs.d/plugins/program/emacs-quickrun")
(require 'quickrun)
;;======================                       quickrun                            =====================


;;======================                        Load yasnippet                          =====================
;;自动补全代码插件(注意，这个插件很消耗时间,用它来补全一些固定格式的代码可以加快开发速度)
(add-to-list 'load-path"~/.emacs.d/plugins/yasnippet")
(require 'yasnippet) ;; not yasnippet-bundle
(setq yas/snippet-dirs '("~/.emacs.d/plugins/yasnippet/snippets" "~/.emacs.d/plugins/yasnippet/extras/imported"))
(yas/initialize)
;;(yas/load-directory "~/.emacs.d/plugins/yasnippet/snippets")
(require 'dropdown-list)
(setq yas/prompt-functions '(yas/dropdown-prompt))
(setq yas/wrap-around-region 'cua)
;;----------------------                        END     yasnippet                       ---------------------


;;======================                       auto complete                            =====================
(add-to-list 'load-path "~/.emacs.d/plugins/auto-complete")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/plugins/auto-complete/dict")
(defun ac-config-default ()
  (setq-default ac-sources '(ac-source-yasnippet ac-source-files-in-current-dir ac-source-abbrev ac-source-dictionary ac-source-words-in-same-mode-buffers))
  (add-hook 'emacs-lisp-mode-hook 'ac-emacs-lisp-mode-setup)
  (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
  (add-hook 'ruby-mode-hook 'ac-ruby-mode-setup)
  (add-hook 'css-mode-hook 'ac-css-mode-setup)
  (add-hook 'auto-complete-mode-hook 'ac-common-setup)
  (global-auto-complete-mode t))
(ac-config-default)

(setq ac-use-menu-map t)
(global-set-key "\M-/" 'auto-complete)

;;======================                       auto complete                            =====================


;;======================                       gtags                       =====================
(autoload 'gtags-mode "gtags" "" t)
(defun gtags-update-single(filename)  
      "Update Gtags database for changes in a single file"
      (interactive)
      (start-process "update-gtags" "update-gtags" "bash" "-c" (concat "cd " (gtags-root-dir) " ; gtags --single-update " filename )))
(defun gtags-update-current-file()
      (interactive)
      (defvar filename)
      (setq filename (replace-regexp-in-string (gtags-root-dir) "." (buffer-file-name (current-buffer))))
      (gtags-update-single filename)
      (message "Gtags updated for %s" filename))
(defun gtags-update-hook()
      "Update GTAGS file incrementally upon saving a file"
      (when gtags-mode
        (when (gtags-root-dir)
          (gtags-update-current-file))))
;;(add-hook 'after-save-hook 'gtags-update-hook)

;;======================                       gtags                       =====================

;;======================                       tail                       =====================
;;统一linux与windows下用tail命令
(require 'tail)
;;======================                       tail                       =====================



