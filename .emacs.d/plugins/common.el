
;;Personal information
(setq user-full-name "sails")
(setq user-mail-address "sailsxu@gmail.com")

;;======================        font  setting            =====================
(set-default-font "Monaco-10")
;;======================     end font setting            =====================

;;======================        time setting            =====================
;;时间显示设置
;;启用时间显示设置
(display-time-mode 1)
;;时间使用24小时制
(setq display-time-24hr-format t)
;;时间显示包括日期和具体时间
(setq display-time-day-and-date t)
;;时间栏旁边启用邮件设置
(setq display-time-use-mail-icon t)
;;时间的变化频率
(setq display-time-interval 10)
;;显示时间，格式如下
(display-time-mode 1)
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)
;;----------------------        END     time setting    ---------------------

;;======================                        Load color-theme                        =====================
;;配色方案,个人感觉在kingsajz,pok-wog,subtle-hacker,wombat,zenburn,blackboard还不错
(add-to-list 'load-path' "~/.emacs.d/plugins/color-theme")
(load-file "~/.emacs.d/plugins/color-theme/color-theme.el")
(load-file "~/.emacs.d/plugins/color-theme/themes/color-theme-darkmate.el")
(load-file "~/.emacs.d/plugins/color-theme/themes/color-theme-wombat.el")
(load-file "~/.emacs.d/plugins/color-theme/themes/color-theme-zenburn.el")
(load-file "~/.emacs.d/plugins/color-theme/themes/color-theme-blackboard.el")
(require 'color-theme)
(color-theme-blackboard)
;;(add-hook 'c-mode-common-hook 'color-theme-blackboard)
(add-hook 'c++-mode-common-hook 'color-theme-zenburn)
;;----------------------                        END     color-theme                     ---------------------


;;以 y/n 替代 yes/no
(fset 'yes-or-no-p 'y-or-n-p)

;;显示括号匹配
(show-paren-mode t)

;;隐藏工具栏
(tool-bar-mode nil)
;; 隐藏滚动条
(scroll-bar-mode nil)

;;高亮显示选中的区域
(transient-mark-mode t)

;;支持emacs和外部程序的拷贝粘贴
(setq x-select-enable-clipboard t)

;;显示80列就换行
(setq default-fill-column 80)

;;显示行号
(global-linum-mode 1)

;;禁止终端响铃
(setq visiable-bell t)

;;在文档最后自动插入空白一行，好像某些系统配置文件是需要这样的
(setq require-final-newline t)
(setq track-eol t)

;;全屏
(defun my-fullscreen ()
  (interactive)
      (x-send-client-message
             nil 0 nil "_NET_WM_STATE" 32
                       '(2 "_NET_WM_STATE_FULLSCREEN" 0)))


;;**********************       winner-mode              *********************
;; 可以使用ctrl-c <-, ctrl-c ->来undo刚刚改变的窗口设置
(when (fboundp 'winner-mode)
  (winner-mode)
  (windmove-default-keybindings))


;;**********************        ibus        *************************
;;因为不想将修改emacs的ctrl-space，所以修改ibus的为shift-space切换
(add-to-list 'load-path "~/.emacs.d/plugins/ibus-el")
(require 'ibus)
(add-hook 'after-init-hook 'ibus-mode-on)


;;**********************        eshell        *************************
(defun clear-shell ()
   (interactive)
   (let ((comint-buffer-maximum-size 0))
     (comint-truncate-buffer)))
(defun clear-eshell ()
  "04Dec2001 - sailor, to clear the eshell buffer."
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)))
