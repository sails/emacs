;;**********************        全局按键设定            *********************

(global-set-key [f11] 'my-fullscreen);F11 全屏
;;习惯设置，打开／关闭菜单
(global-set-key [f12] 'menu-bar-mode)
;;eshell/shell clear
(defun my-shell-hook ()
      (local-set-key "\C-cl" 'clear-shell))
(add-hook 'shell-mode-hook 'my-shell-hook)
(defun my-eshell-hook ()
      (local-set-key "\C-cl" 'clear-eshell))
(add-hook 'eshell-mode-hook 'my-eshell-hook)
