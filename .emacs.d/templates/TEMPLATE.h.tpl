/*
 * =================================================================================
 *
 *       Filename:  (>>>FILE<<<)
 *
 *    Description:  (>>>P<<<) 
 *
 *        Version:  1.0
 *        Created:  (>>>VC_DATE<<<)
 *       Revision:  (>>>1<<<)
 *       Compiler:  gcc
 *
 *         Author:  (>>>AUTHOR<<<)
 *
 * =================================================================================
 */


#ifndef _(>>>FILE_UPCASE<<<)_H
#define _(>>>FILE_UPCASE<<<)_H

#ifdef __cplusplus
extern "C" {
#endif

(>>>2<<<)


#ifdef __cplusplus
}
#endif

#endif /* _(>>>FILE_UPCASE<<<)_H */
